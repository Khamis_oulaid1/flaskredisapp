import json

from flask import Flask, request, jsonify
import psycopg2
from marshmallow import Schema, fields, ValidationError
import redis
# tracing
from jaeger_client import Config
from flask_opentracing import FlaskTracer
import opentracing

redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)

app = Flask(__name__)

connection = psycopg2.connect(database="root",
                              user="root",
                              password="root",
                              host="localhost", port="5432")


def initialize_tracer():
    config = Config(
        config={
            'sampler': {'type': 'const', 'param': 1}
        },
        service_name='book_crud')
    return config.initialize_tracer()


flask_tracer = FlaskTracer(initialize_tracer, True, app)

"""parent_span = flask_tracer.get_span()
with opentracing.tracer.start_span('child1', child_of=parent_span) as span:
    span.set_tag('test', 'hello world')"""


#  Define the Schema with Marshmallow
class BookSchema(Schema):
    title = fields.Str(required=True)
    author = fields.Str(required=True)
    genre = fields.Str(required=True)
    price = fields.Float(required=True)


# get methode
@app.get('/books')
def get_books():
    parent_span = flask_tracer.get_span()
    with opentracing.tracer.start_span('Get method', child_of=parent_span) as span:
        span.set_tag(' tag get result ', 'teg get ')
        span.log_kv({' log get test': 'log get'})
        cached_books = redis_client.get('books')
        if cached_books:
            print("cached books from redis are ", cached_books)
            return jsonify(json.loads(cached_books))
        else:
            print("no cach found")
        with connection:
            with connection.cursor() as cursor:
                try:
                    cursor.execute("SELECT * FROM books")
                    books = cursor.fetchall()
                    print("Fetched books are:", books)
                    bookSchema = BookSchema(many=True)
                    result = bookSchema.dump(books)
                    print("Serialized result:", result)
                    redis_client.set('books', json.dumps(books), 100)
                    return jsonify(books), 200
                except psycopg2.Error as e:
                    return jsonify({'error': str(e)}), 500
                finally:
                    cursor.close()

        connection.close()




# post methode
@app.post('/books')
def create_book():
    parent_span = flask_tracer.get_span()
    with opentracing.tracer.start_span('Post method', child_of=parent_span) as span:
        span.set_tag(' tag post result ', 'post tag ')
        span.log_kv({' log post test': 'post log '})
        data = request.get_json()
        # check data
        if not data:
          return {'message': 'No input data provided'}, 400

        # Validate data
        schema = BookSchema()
        try:
          data = schema.load(data)
        except ValidationError as err:
           return jsonify(err.messages), 422
        # deal with the valid data (save to db)
        with connection:
          with connection.cursor() as cursor:
            try:
                cursor.execute(
                    """CREATE TABLE IF NOT EXISTS books (id serial PRIMARY KEY, title varchar(100),author varchar(100),genre varchar(100),price float);""")
                cursor.execute("""INSERT INTO books (title, author, genre,price) VALUES (%s, %s, %s, %s);""",
                               (data['title'], data['author'], data['genre'], data['price']))
                connection.commit()
                cached_books = redis_client.delete('books')
                print('cach is deleted value :', cached_books)
                return jsonify({'message': 'Book added successfully'}), 201
            except psycopg2.IntegrityError as err:
                return jsonify(err), 500
            finally:
                cursor.close()
        connection.close()


# update
@app.put('/books/<int:book_id>')
def update_book(book_id: int):
    parent_span = flask_tracer.get_span()
    with opentracing.tracer.start_span('Update method', child_of=parent_span) as span:
        span.set_tag(' tag update result ', 'update tag ')
        span.log_kv({' log update test': 'update  log '})
        data = request.get_json()
        if not data:
           return {'message': 'No input data provided'}, 400

        # validate the data
        schema = BookSchema()
        try:
           data = schema.load(data)
        except ValidationError as err:
           return jsonify(err.messages), 422

        with connection:
          with connection.cursor() as cursor:
            try:
                cursor.execute("""UPDATE books SET title = %s, author = %s, genre = %s, price =%s WHERE id = %s;""",
                               (data.get("title"), data.get("author"), data.get("genre"), data.get("price"), book_id))
                connection.commit()
                cached_books = redis_client.delete('books')
                return jsonify({'message': 'book updated successfully'}), 200
            except psycopg2.Error as err:
                return jsonify(err.messages), 500

            finally:
                cursor.close()
        connection.close()


# delete
@app.delete('/books/<int:book_id>')
def delete_book(book_id: int):
    parent_span = flask_tracer.get_span()
    with opentracing.tracer.start_span('Delete method', child_of=parent_span) as span:
        span.set_tag(' tag delete result ', 'delete tag ')
        span.log_kv({' log delete test': 'delete log '})
        with connection:
          with connection.cursor() as cursor:
            try:
                cursor.execute("""DELETE FROM books WHERE id=%s""", (book_id,))
                connection.commit()
                cached_books = redis_client.delete('books')
                return jsonify({'message': 'Book  deleted successfully'}), 200
            except psycopg2.Error as err:
                return jsonify(err), 500
            finally:
                cursor.close()

        connection.close()


if __name__ == '__main__':
    app.run(debug=True)
